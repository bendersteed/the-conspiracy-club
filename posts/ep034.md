title: Επεισόδιο 34: Τα απόκρυφα των Ναζί
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 34
date: 2021-02-24 19:00
tags: nazi, occult, astrology
summary: Ποιες οι επαφές των Ναζί με τα απόκρυφα μυστήρια; Αστρολόγοι, αρχαίες γερμανικές θεότητες, ινδικοί μύθοι, ταξίδια στο Θιβέτ και αναζητήσεις χαμένων θησαυρών. Σε αυτό το επεισόδιο μελετάμε τις μυστήριες ιδέες των ηγετών του Ναζισμού.
cover: https://theconspiracyclub.gr/assets/images/ep34.jpg
duration: "55:13"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep34.mp3" length:"26506051"
---

Ποιες οι επαφές των Ναζί με τα απόκρυφα μυστήρια; Αστρολόγοι, αρχαίες γερμανικές θεότητες, ινδικοί
μύθοι, ταξίδια στο Θιβέτ και αναζητήσεις χαμένων θησαυρών. Σε αυτό το επεισόδιο μελετάμε τις
μυστήριες ιδέες των ηγετών του Ναζισμού.

## Σχετικά links:
1. [Nazis - the occult conspiracy - youtube.com](https://www.youtube.com/watch?v=vFIGJLcjMYI)
2. [The Nazis as occult masters? It’s a good story but not history - aeon.co ](https://aeon.co/ideas/the-nazis-as-occult-masters-its-a-good-story-but-not-history)
3. [Occulitsm in Nazism - wikipedia.org](https://en.wikipedia.org/wiki/Occultism_in_Nazism)
