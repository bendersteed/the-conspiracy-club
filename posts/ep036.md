title: Επεισόδιο 36: Scientology
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 36
date: 2021-03-08 14:00
tags: cult, scientology, aliens
summary: Τι είναι τελικά η Σαηεντολογία; Ο καλεσμένος μας, με το ψευδώνυμο Πέτρος, έρχεται μαζί μας για να μας μιλήσει για την εμπειρία του ως μέλος της Σαηεντολογίας και να περιγράφουμε τα μυστήρια μιας από τις πιο διαδεδομένες και προβεβλημένες αιρέσεις. 
cover: https://theconspiracyclub.gr/assets/images/ep36.jpg
duration: "01:11:05"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep36.mp3" length:"34124994"
---

Τι είναι τελικά η Σαηεντολογία; Ο καλεσμένος μας, με το ψευδώνυμο Πέτρος, έρχεται μαζί μας για να
μας μιλήσει για την εμπειρία του ως μέλος της Σαηεντολογίας και να περιγράφουμε τα μυστήρια μιας από
τις πιο διαδεδομένες και προβεβλημένες αιρέσεις.

![Ο Πέτρος έξω από τα γραφεία της Σαηεντολογίας στην Κοπεγχάγη](https://theconspiracyclub.gr/assets/images/ep36-1.jpg)

## Σχετικά links:
1. [Scientology beliefs and practices - wikipedia.org](https://en.wikipedia.org/wiki/Scientology_beliefs_and_practices)
2. [Church of Scientology - wikipedia.org](https://en.wikipedia.org/wiki/Church_of_Scientology)
3. [Xenu - wikipedia.org](https://en.wikipedia.org/wiki/Xenu)
4. [What is Scientology? - cnn.com](https://edition.cnn.com/2017/03/22/us/believer-what-is-scientology/index.html)
5. [What happens when you try to leave the Church of Scientology? - theguardian.com](https://www.theguardian.com/world/2011/apr/23/try-to-leave-church-scientology-lawrence-wright)
6. [How Scientology changed the internet - bbc.com](https://www.bbc.com/news/technology-23273109)
[Scientology, Stem Cells, and Exosomes? - regenexx.com](https://regenexx.com/blog/scientology-stem-cells-and-exosomes/#gref)
7. [20 Celebrities Who Left Scientology - cafemon.com](https://cafemom.com/entertainment/225900-celebrities-who-left-scientology)
8. [Scientology: The thriving cult of greed and power - cs.emu.edu](https://www.cs.cmu.edu/~dst/Fishman/time-behar.html)
9. [Project Chanology - wikipedia.org](https://en.wikipedia.org/wiki/Project_Chanology)
