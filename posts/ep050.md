title: Επεισόδιο 50: Μάχες Κρυπτοειδών Ι: Μεγαλοπόδαρος εναντίον Τσουπακάμπρα
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 50
date: 2021-11-15 19:45
tags: cryptozoology, cryptids, bigfoot, chupacabra
summary: Σε αυτό επεισόδιο ανοίγουμε ένα μεγάλο κεφάλαιο των συνωμοσιών: την Κρυπτοζωολογία! Ξεκινάμε το ταξίδι μας μελετώντας δυο κρυπτοειδή από την ήπειρο της Αμερικής, το Μεγαλοπόδαρο και το Τσουπακάμπρα. Ποια τα χαρακτηριστικά τους; Ποιο θα έβγαινε νικητής σε μια μάχη μέχρι θανάτου;
cover: https://theconspiracyclub.gr/assets/images/ep50.jpg
duration: "53:36"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep50.mp3" length:"25735129"
---

Σε αυτό επεισόδιο ανοίγουμε ένα μεγάλο κεφάλαιο των συνωμοσιών: την Κρυπτοζωολογία! Ξεκινάμε το
ταξίδι μας μελετώντας δυο κρυπτοειδή από την ήπειρο της Αμερικής, το Μεγαλοπόδαρο και το
Τσουπακάμπρα. Ποια τα χαρακτηριστικά τους; Ποιο θα έβγαινε νικητής σε μια μάχη μέχρι θανάτου;

## Σχετικά links:
1. [Bigfoot - wikipedia.org](https://en.wikipedia.org/wiki/Bigfoot)
2. [Sasquatch - britannica.com](https://www.britannica.com/topic/Sasquatch)
3. [Sasquatch - oregonwild.org](https://oregonwild.org/wildlife/sasquatch)
4. [Meet the Bigfoot experts who took Jeff Goldblum Sasquatch hunting on Mount Shasta for Disney Plus - sfgate.com](https://www.sfgate.com/sf-culture/article/Jeff-Goldblum-went-Bigfoot-hunting-16613153.php)
5. [Chupacabra - wikipedia.org](https://en.wikipedia.org/wiki/Chupacabra)
6. [Chupacabra - wikipedia.org](https://www.britannica.com/topic/chupacabra)
7. [Chupacabra Science: How Evolution Made a Mythical Monster - nationalgeographic.com](https://www.nationalgeographic.com/culture/article/101028-chupacabra-evolution-halloween-science-monsters-chupacabras-picture)
8. [How Chupacabras Work - science.howstuffworks.com](https://science.howstuffworks.com/science-vs-myth/strange-creatures/chupacabra.htm)
