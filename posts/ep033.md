title: Επεισόδιο 33: UFO στην Ελλάδα
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 33
date: 2021-02-15 16:00
tags: ufo, greece, aliens
summary: Η χώρα μας, παρότι μικρή, έχει μεγάλη παράδοση στη θέαση αγνώστης ταυτότητας ιπτάμενων αντικειμένων (ΑΤΙΑ). Σας παρουσιάζουμε τις πιο ενδιαφέρουσες από αυτές τις περιπτώσεις, καταλήγοντας στο ελληνικό Roswell.
cover: https://theconspiracyclub.gr/assets/images/ep33.jpg
duration: "59:28"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep33.mp3" length:"28548060"
---

Η χώρα μας, παρότι μικρή, έχει μεγάλη παράδοση στη θέαση αγνώστης ταυτότητας ιπτάμενων αντικειμένων
(ΑΤΙΑ). Σας παρουσιάζουμε τις πιο ενδιαφέρουσες από αυτές τις περιπτώσεις, καταλήγοντας στο ελληνικό
Roswell.


## Σχετικά links:
1. [Δηλώσεις πρώης αρχηγού διαστήματος Ισραήλ - news247.gr](https://www.news247.gr/kosmos/proin-archigos-diastimatos-israil-oi-exogiinoi-yparchoyn-den-eimaste-etoimoi-gia-aytoys.9074311.html?utm_source=Sport24&utm_medium=BestofNetwork_home&utm_campaign=24MediaWidget&utm_term=Pos2)
2. [Haim Eshed - wikipedia.org](https://en.wikipedia.org/wiki/Haim_Eshed)
3. [huforc.gr](https://web.archive.org/web/20140517135227/http://huforc.gr/)
4. [Ghost Rockets - wikipedia.org](https://en.wikipedia.org/wiki/Ghost_rockets)
11. [Ιπτάμενα αντικείμενα στον Ταϋγετο - youtube.com](https://www.youtube.com/watch?v=Bnozl2oWz2U)
12. [Οι σημαντικότερες αναφορές για ΑΤΙΑ στην Ελλάδα - omadaorfeas.blogspot.com](https://omadaorfeas.blogspot.com/2020/03/shmantikoteres-anafores-atia-ellada.html)
6. [Η περίεργη ιστορία της πιο γνωστής περίπτωσης απαγωγής ανθρώπου από εξωγήινους στην ελλάδα - vice.com](https://www.vice.com/el/article/j5zb34/h-periergh-istoria-ths-pio-gnwsths-periptwshs-apagwghs-an8rwpoy-apo-e3wghinoys-sthn-ellada)
7. [Το περιστατικό της Αταλάντης - e-sterea.gr](https://e-sterea.gr/index.php/nea/ellada/31499-to-peristatiko-tis-atalantis-to-1990-video)
8. [Αταλάντη UFO: Όταν τα ΑΤΙΑ επισκέφθηκαν την Ελλάδα (και το έκρυψαν) - fimes.gr](https://www.fimes.gr/2016/01/atalanti-ufo-atia-ellada/)
9. [ΑΤΙΑ και εξωγήινοι στην Ελλάδα - youtube.com](https://www.youtube.com/watch?v=XFg5kdWhyX8)
10. [Το περιστατικό της Αταλάντης - youtube.com](https://www.youtube.com/watch?v=K7rQPvrmUZg)
11. [Ελληνίδα ηθοποιός εξομολογήθηκε τις δυο στενές επαφές της - ufotruth-gr.blogspot.com](https://ufotruth-gr.blogspot.com/2021/02/blog-post.html)
12. [Όταν τα UFO εμφανίστηκαν στην Ελλάδα - news247.gr](https://www.news247.gr/afieromata/otan-ta-ufo-emfanistikan-stin-ellada.6282137.html)
14. [Οι σημαντικότερες εμφανίσεις UFO στην Ελλάδα - statusradio.gr](https://statusradio.gr/2014/07/oi-shmantikoteres-emfaniseis-ufo-sthn-el/)
15. [UFO - vault.fbi.gov](https://vault.fbi.gov/UFO)

