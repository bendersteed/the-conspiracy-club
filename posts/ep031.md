title: Επεισόδιο 31: Ενάντια στα εμβόλια
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 31
date: 2021-02-01 18:00
tags: vaccines, anti-vaxx, conspiracy, covid
summary: Από την πρώτη εμφάνιση των εμβολίων, μέρος του πληθυσμού είχε αμφιβολίες για τη λειτουργία και τις επιπτώσεις των εμβολίων. Σε αυτό το επεισόδιο μελετάμε τις κυριότερες θεωρίες των σύγχρονων antivaxxers.
cover: https://theconspiracyclub.gr/assets/images/ep31.jpg
duration: "55:27"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep31.mp3" length:"26620018"
---

Από την πρώτη εμφάνιση των εμβολίων, μέρος του πληθυσμού είχε αμφιβολίες για τη λειτουργία και τις
επιπτώσεις των εμβολίων. Σε αυτό το επεισόδιο μελετάμε τις κυριότερες θεωρίες των σύγχρονων
antivaxxers.

> Εκ παραδρομής αναφέραμε πως το συντηρητικό [Thiomersal](https://en.wikipedia.org/wiki/Thiomersal)
> περιέχει μόλυβδο. Στην πραγματικότητα περιέχει υδράργυρο. Για περισσότερα περί του θέματος
> διαβάστε το άρθρο στην wikipedia. 

## Σχετικά links:
1. [Andrew Wakefield - wikipedia.org](https://en.wikipedia.org/wiki/Andrew_Wakefield)
2. [Vaccine hesitancy - wikipedia.org](https://en.wikipedia.org/wiki/Vaccine_hesitancy)
3. [The online anti-vaccine movement in the age of COVID-19 - thelancet.com](https://www.thelancet.com/journals/landig/article/PIIS2589-7500(20)30227-2/fulltext)
4. [A history of the Anti-vaxxer movement - eurekalert.org](https://www.eurekalert.org/pub_releases/2020-09/tmp-aho092220.php)
5. [The discredited doctor hailed by the anti-vaccine movement - nature.com](https://www.nature.com/articles/d41586-020-02989-9)
6. [How Bill Gates became the voodoo doll of Covid conspiracies - bbc.com](https://www.bbc.com/news/technology-52833706)
7. [The top 10 bombshell VACCINE stories of 2019 – naturalnews.com](https://www.naturalnews.com/2019-12-12-top-10-bombshell-vaccine-stories-of-2019.html)
8. [ID2020 | Digital Identity Alliance](https://id2020.org/)
9. [Andrew Wakefield - rationalwiki.org](https://rationalwiki.org/wiki/Andrew_Wakefield#The_Lancet_study)
10. [Το εμβόλιο της Pfizer και οι εξωφρενικές θεωρίες συνωμοσίας για τα…
τσιπάκια – tovima.gr](https://www.tovima.gr/2020/12/18/science/to-emvolio-tis-pfizer-kai-oi-eksofrenikes-theories-synomosias-gia-ta-tsipakia-ayta-einai-ta-systatika-pou-periexei/)
11. [Η λίστα του BBC με τις... κορυφαίες θεωρίες συνομωσίας για τα εμβόλια
κατά του κορωνοϊού - eleftherostypos.gr](https://eleftherostypos.gr/diethni/i-lista-tou-bbc-me-tis-koryfaies-theories-synomosias-gia-ta-embolia-kata-tou-koronoiou/)
12. [Πανδημία… θεωριών συνωμοσίας για τα εμβόλια | kathimerini.gr](https://www.kathimerini.gr/society/561193585/pandimia-theorion-synomosias-gia-ta-emvolia/)
13. [Gulf War syndrome - wikipedia.org](https://en.wikipedia.org/wiki/Gulf_War_syndrome)
14. [History of the Anti-Vaccine Movement - verywellhealth.com](https://www.verywellhealth.com/history-anti-vaccine-movement-4054321)
