title: Επεισόδιο 65: 2000 χρόνια Conspiracy Club
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 65
date: 2022-03-22 00:24
tags: birthday, soy, soy boy, feminization
summary: Επετειακό επεισόδιο μέρος 2, μετά από 2 ολόκληρα χρόνια podcasting. Τι επιφυλάσει το μέλλον στη Λέσχη των Συνωμοσιών; Επιπλέον δώρο θέμα: Ευθύνεται η σόγια για την εξάλειξη της ανδροπρέπειας; 
cover: https://theconspiracyclub.gr/assets/images/ep65.jpg
duration: "00:53:39"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep65.mp3" length:"25752611"
---
Επετειακό επεισόδιο μέρος 2, μετά από 2 ολόκληρα χρόνια podcasting. Τι επιφυλάσει το μέλλον στη
Λέσχη των Συνωμοσιών; 

Bonus δώρο θέμα: Ευθύνεται η σόγια για την εξάλειξη της ανδροπρέπειας;

## Σχετικά links:
1. [Soy boy - RationalWiki](https://rationalwiki.org/wiki/Soy_boy)
2. [An Anatomy of the Soy Boy // New Socialist](https://newsocialist.org.uk/an-anatomy-of-the-soy-boy/)
3. [Latest right-wing conspiracy theory: soya will make your kids gay! - The F-Word](https://thefword.org.uk/2007/12/latest_rightwin/)
4. [Inside the "soy boy" conspiracy theory: It combines misogyny and the warped world of pseudosciece | Salon.com](https://www.salon.com/2018/11/14/the-soy-boy-conspiracy-theory-alt-right-thinks-left-wing-has-it-out-for-them-with-soybeans_partner/)
5. [Soy boy - Wikipedia](https://en.wikipedia.org/wiki/Soy_boy)
6. [Why Men Are Afraid Soy Will Turn Them Into Women - The Atlantic](https://www.theatlantic.com/health/archive/2020/02/why-men-are-afraid-soy-will-turn-them-women/605968/)
7. [The Soy Conspiracy - Chinese For Labour](https://www.esea4labour.org/the_soy_conspiracy)
8. [SOY BOYS: A MEASURED RESPONSE - YouTube](https://www.youtube.com/watch?v=C8dfiDeJeDU)
