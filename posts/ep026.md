title: Επεισόδιο 26: Εμφανίσεις και εξαφανίσεις
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 26
date: 2020-12-01 14:00
tags: dimensions, strange, mystery
summary: Επισκέπτες από άλλες διαστάσεις ή μελετημένες απάτες; Η Δώρα έρχεται μαζί μας για να συζητήσουμε για περιπτώσεις ανθρώπων που δήλωσαν πως έρχονται από ανύπαρκτα μέρη ή είναι εγκλωβισμένα σε άλλες διαστάσεις, τα ρήγματα της καθημερινότητας, σκύλους στα μπαλκόνια και τα κομμένα ταμπελάκια από ρούχα.
cover: https://theconspiracyclub.gr/assets/images/ep26.jpg
duration: "58:43"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep26.mp3" length:"28185754"
---

Επισκέπτες από άλλες διαστάσεις ή μελετημένες απάτες; Η Δώρα έρχεται μαζί μας για να συζητήσουμε για
περιπτώσεις ατόμων που δήλωσαν πως έρχονται από ανύπαρκτα μέρη ή είναι εγκλωβισμένα σε άλλες
διαστάσεις, τα ρήγματα της καθημερινότητας, σκύλους στα μπαλκόνια και τα κομμένα ταμπελάκια από
ρούχα. 


## Σχετικά links:
1. [The man from Taured: A mystery of a man with no country | Science   101](https://www.science101.com/the-man-from-taured/)
2. [Taured Mystery: The Man Who Vanished As Mysteriously As He Came - News Nation English](https://english.newsnationtv.com/offbeat/news/taured-mystery-the-man-who-vanished-253965.html)
3. [The Mysterious Tale of the Man from Taured - Ancient Origins](https://www.ancient-origins.net/unexplained-phenomena/mysterious-tale-man-taured-evidence-parallel-universes-or-embellishment-005788)
4. [The mysterious man from Taured: was an interdimensional traveler?](https://www.fanwave.it/en/mysteries/877-mysterious-man-from-taured-an-interdimensional-traveler.html)
5. [TAURED. LAXARIA. LIZBIA. - The Ultimate Secrets](https://theultimatesecrets.wordpress.com/2015/11/09/taured-laxaria-lizbia/)
6. [reddit thread on the man from   Taured](https://www.reddit.com/r/UnresolvedMysteries/comments/1zsyz2/on_july_1954_a_man_arrives_at_tokyo_airport_in/)
7. [Beneath the Stains of Time: The Man from Taured - moonlight-detective](http://moonlight-detective.blogspot.com/2015/04/the-man-from-taured.html)
8. [Unresolved: Who Was The Man From Taured — theghostinmymachine.com](https://theghostinmymachine.com/2015/07/06/unresolved-who-was-the-man-from-taured-and-did-he-even-exist-at-all/)
9. [First ever known reference on the man from Taured](https://i.imgur.com/lWDjuk2.jpg)
10. [Jophar Vorin — 19th Century Man of Mystery – The Elloe Recorder](https://francisbarkerart.com/2020/08/04/jophar-vorin-19th-century-man-of-mystery/)
11. [Jophar Vorin – Mysteriesrunsolved](https://mysteriesrunsolved.com/2018/07/lost-stranger-jophar-vorin-time-travel-story.html)
12. [Jophar Vorin, The Man from Laxaria - Conspiracy Theories Archives](https://conspiracytheoriesarchives.com/blog/post/jophar-vorin-man-laxaria)
13. [Lerina Garcia Gordo, the woman from an alternate universe - Conspiracy Theories Archives](https://conspiracytheoriesarchives.com/blog/post/lerina-garcia-gordo-woman-alternate-universe)
14. [Aangirfan: Lerina Garcia; Max Loughan; parallel universes; god; groundhog day - aanirfan.blogspot.com](http://aanirfan.blogspot.com/2017/03/lerina-garcia-max-loughan-parallel.html)
15. [The Mysterious Case of Lerina Garcia Gordo - anomalien.com](https://anomalien.com/the-mysterious-case-of-lerina-garcia-gordo-woman-from-a-parallel-universe/)
16. [Woman Claims To Be From A Parallel Universe – Ghost Theory](http://www.ghosttheory.com/2012/01/12/woman-claims-she-is-from-a-parallel-universe)
17. [The Mysterious Appearance of Peter Bergmann – Ghost Theory](http://www.ghosttheory.com/2015/12/16/the-mysterious-appearance-of-peter-bergmann)
18. [Peter Bergmann case - Wikipedia](https://en.wikipedia.org/wiki/Peter_Bergmann_case)
19. [Tamam Shud case - Wikipedia](https://en.wikipedia.org/wiki/Tamam_Shud_case#Connection_to_the_Rubaiyat_of_Omar_Khayyam)
