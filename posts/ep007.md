title: Επεισόδιο 7: Το υδροκίνητο αυτοκίνητο του Stanley Meyer
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 7
date: 2020-04-15 23:00
tags: oil, corporate, conspiracy
summary: Ο Δημήτρης μας λέει την ιστορία του Stanley Meyer. Τα συμφέροντα γύρω από το πετρέλαιο, ένας μυστήριος θάνατος και μια μεγάλη κουβέντα για το τι είναι συνωμοσία και τι πραγματικότητα στο έτος 2020.
cover: https://theconspiracyclub.gr/assets/images/ep07.jpg
duration: "46:12"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep07.mp3" length:"44361133"
---

Ο Δημήτρης μας λέει την ιστορία του Stanley Meyer. Τα συμφέροντα γύρω
από το πετρέλαιο, ένας μυστήριος θάνατος και μια μεγάλη κουβέντα για
το τι είναι συνωμοσία και τι πραγματικότητα στο έτος 2020.


## Σχετικά links:
1. [Stan Meyer explains the Water Fuel Technology - youtube.com](https://www.youtube.com/watch?v=staL1wr07Sg)
2. [Stan Meyer - Water Car Inventor Killed](https://www.youtube.com/watch?v=awO3--KQKng)
3. [Process and apparatus for the production of fuel gas and the enhanced release of thermal energy from such gas - google patents](https://www.google.com/patents/US5149407)
4. [Method for the production of a fuel gas - google patents](https://patents.google.com/patent/US4936961)
5. [Controlled process for the production of thermal energy from gases and apparatus useful therefore - google patents](https://patents.google.com/patent/US4826581)
6. [Eye witness accounts to Meyer's invention that transforms water into a source of fuel - youtube.com](https://www.youtube.com/watch?v=EkjpVcsRQLc)
7. [Inventor Of ‘Water-Powered Car’ Died Screaming ‘They Poisoned Me’ - unilad.co.uk](https://www.unilad.co.uk/life/inventor-of-water-powered-car-died-screaming-they-poisoned-me/)
8. [The Mysterious Death of Stanley Meyer and His Water-Powered Car - gaia.com](https://www.gaia.com/article/the-mysterious-death-of-stanley-meyer-and-his-water-powered-car)
9. [The car that ran on water - dispatch.com](https://web.archive.org/web/20080214190405/http:/www.dispatch.com/live/content/local_news/stories/2007/07/08/hydroman.ART_ART_07-08-07_A1_4V77MOK.html)
10. [Michael Laughton, member of the British Energy Association - worldenergy.com](https://web.archive.org/web/20060927044324/http://www.worldenergy.org/wec-geis/news_events/member_news/BEA_WS_0205.asp)
11. [Water Fuel-Cell Inventor Murdered by Government - youtube.com](https://www.youtube.com/watch?v=gIAZFU2rAQE)
12. [Official Stanley Meyer Autopsy Report - youtube.com](https://www.youtube.com/watch?v=C3FQhf8X9OY)
