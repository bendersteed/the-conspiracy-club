title: Επεισόδιο 55: Το νησί του Πάσχα
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 55
date: 2021-12-24 22:30
tags: easter island, aliens, moai
summary: Ένα μοναχικό νησί στη μέση του Ειρηνικού Ωκεανού, στολισμένο με τεράστια αγάλματα και μυστήρια σύμβολα. Η χαμένη στο χρόνο ιστορία ενός από τα πιο μυστήρια μέρη στον κόσμο, αυτού που οι λευκοί ονόμασαν, ατυχώς, νησί του Πάσχα.
cover: https://theconspiracyclub.gr/assets/images/ep55.jpg
duration: "01:06:28"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep55.mp3" length:"31908704"
---
Ένα μοναχικό νησί στη μέση του Ειρηνικού Ωκεανού, στολισμένο με τεράστια αγάλματα και μυστήρια
σύμβολα. Η χαμένη στο χρόνο ιστορία ενός από τα πιο μυστήρια μέρη στον κόσμο, αυτού που οι λευκοί
ονόμασαν, ατυχώς, νησί του Πάσχα.

## Σχετικά links:
1. [Easter Island Mystery Solved? New Theory Says Giant Statues Rocked](https://www.nationalgeographic.com/culture/article/120622-easter-island-statues-moved-hunt-lipo-science-rocked) 
2. [Conspiracy Theories and Legends: Easter Island — Steemit](https://steemit.com/history/@vitruvianman/conspiracy-theories-and-legends-easter-island) 
3. [Easter Island - Wikipedia](https://en.wikipedia.org/wiki/Easter_Island#Statues) 
4. [Hanau epe - Wikipedia](https://en.wikipedia.org/wiki/Hanau_epe) 
5. [The Secrets of Easter Island | History | Smithsonian Magazine](https://www.smithsonianmag.com/history/the-secrets-of-easter-island-59989046/) 
6. [What happened on Easter Island? New theory proposed - CNN](https://edition.cnn.com/2018/08/13/world/easter-island-statue-study-intl/index.html) 
7. [NOVA Online | Secrets of Easter Island | Your Theories](https://www.pbs.org/wgbh/nova/easter/move/theories.html) 
8. [10 Fascinating Theories Surrounding Easter Island - Listverse](https://listverse.com/2014/08/19/10-fascinating-theories-surrounding-easter-island/) 
9. [Göbekli Tepe - Wikipedia](https://en.wikipedia.org/wiki/G%C3%B6bekli_Tepe)
