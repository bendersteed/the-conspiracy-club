title: Επεισόδιο 81: Σελήνη
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 81
date: 2022-10-11 13:00
tags: moon, lunar, aliens, ancient
summary: Ο Κώστας και η Μαρία έρχονται στο σημερινό επεισόδιο για να μας μιλήσουν για το φεγγάρι, την περίεργη ιστορία του και το ακόμα πιο μυστήριο εσωτερικό του. Είναι το φεγγάρι τεχνητό; Υπάρχει ζωή στο φεγγάρι;
cover: https://theconspiracyclub.gr/assets/images/ep81.jpg
duration: "01:03:37"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep81.mp3" length:"30543816"
---
Ο Κώστας και η Μαρία έρχονται στο σημερινό επεισόδιο για να μας
μιλήσουν για το φεγγάρι, την περίεργη ιστορία του και το ακόμα πιο
μυστήριο εσωτερικό του. Είναι το φεγγάρι τεχνητό; Υπάρχει ζωή στο
φεγγάρι;

## Σχετικά links:
1. [Τι έλεγαν οι αρχαίοι Έλληνες φιλόσοφοι για την Σελήνη - diodos.gr](https://www.diodos.gr/%CE%AD%CF%81%CE%B5%CF%85%CE%BD%CE%B1-%CE%B3%CE%BD%CF%8E%CF%83%CE%B7/%CE%B3%CE%BD%CF%8E%CF%83%CE%B5%CE%B9%CF%82-%CE%AD%CF%81%CE%B5%CF%85%CE%BD%CE%B1/item/%CF%84%CE%B9-%CE%AD%CE%BB%CE%B5%CE%B3%CE%B1%CE%BD-%CE%BF%CE%B9-%CE%B1%CF%81%CF%87%CE%B1%CE%AF%CE%BF%CE%B9-%CE%AD%CE%BB%CE%BB%CE%B7%CE%BD%CE%B5%CF%82-%CF%86%CE%B9%CE%BB%CF%8C%CF%83%CE%BF%CF%86%CE%BF%CE%B9-%CE%B3%CE%B9%CE%B1-%CF%84%CE%B7%CE%BD-%CF%83%CE%B5%CE%BB%CE%AE%CE%BD%CE%B7.html)
2. [Επιβεβαιώθηκε επίσημα από τη NASA; - ellinikahoaxes.gr](https://www.ellinikahoaxes.gr/2015/09/23/nasa-selini-ktiria-arxaoiellines/)
3. [10 Astonishing Facts About the Moon - popularmechanics.com](https://www.popularmechanics.com/space/moon-mars/g28448808/disproven-moon-facts/)
4. [Hollow Moon and Spaceship Moon Theories - donnatheastronomer.com.au](https://donnatheastronomer.com.au/2019/06/29/hollow-moon-and-spaceship-moon-theories/?v=6cc98ba2045f)
