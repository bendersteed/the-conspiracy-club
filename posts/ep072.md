title: Επεισόδιο 72: Το μυστήριο του Pont-Saint-Esprit
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 72
date: 2022-06-16 13:00
tags: mkultra, lsd, cia, france, pont-saint-esprit
summary: Το 1951, σε μια μικρή πόλη της Γαλλίας ξεσπά μια μυστηριώδης πάθηση με περίεργα ψυχολογικά και σωματικά συμπτώματα. Ατύχημα ή κάτι πιο σκοτεινό;
cover: https://theconspiracyclub.gr/assets/images/ep72.jpg
duration: "00:54:37"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep72.mp3" length:"26218507"
---
Το 1951, σε μια μικρή πόλη της Γαλλίας ξεσπά μια μυστηριώδης πάθηση με περίεργα ψυχολογικά και
σωματικά συμπτώματα. Ατύχημα ή κάτι πιο σκοτεινό;

## Σχετικά links:
1. [1951 Pont-Saint-Esprit mass poisoning -
   wikipedia.org](https://en.wikipedia.org/wiki/1951_Pont-Saint-Esprit_mass_poisoning#Background)
2. [When the French Village of Pont-Saint-Esprit Went Temporarily Mad -
   mentalfloss.com](https://www.mentalfloss.com/article/558020/pont-saint-esprit-france-1951-bread-poisoning-mass-hallucinations)
3. [Did the CIA poison a French town with LSD? -
   france24.com](https://www.france24.com/en/20100311-did-cia-poison-french-town-with-lsd)
4. [Pont-Saint-Esprit poisoning: Did the CIA spread LSD? - BBC News -
   bbc.com](https://www.bbc.com/news/world-10996838)
5. [Aspergillus fumigatus - wikipedia.org](https://en.wikipedia.org/wiki/Aspergillus_fumigatus#Nutrient_acquisition)
