title: Επεισόδιο 75: DB Cooper
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 75
date: 2022-07-07 00:30
tags: aero, hijacking, cooper, db, mystery
summary: Το χρονικό μιας από τις πιο μυστήριες αεροπειρατίες που έγιναν ποτέ. Ποιος ο αινιγματικός κ. Cooper και πως κατάφερε να πραγματοποιήσει μια από τις ελάχιστες επιτυχημένες αεροπειρατίες;
cover: https://theconspiracyclub.gr/assets/images/ep75.jpg
duration: "53:52"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep75.mp3" length:"25857524"
---
Το χρονικό μιας από τις πιο μυστήριες αεροπειρατίες που έγιναν ποτέ. Ποιος ο αινιγματικός κ. Cooper
και πως κατάφερε να πραγματοποιήσει μια από τις ελάχιστες επιτυχημένες αεροπειρατίες;

## Σχετικά links:
1. [Yet Another D.B. Cooper Theory: He Survived And Landed In ... Cle Elum? - Northwest Public Broadcasting](https://www.nwpb.org/2018/07/25/yet-another-d-b-cooper-theory-he-survived-and-landed-in-cle-elum/)
2. [D.B. Cooper: Flight Attendant Tina Mucklow Opens Up - Rolling Stone](https://www.rollingstone.com/culture/culture-features/db-cooper-tina-mucklow-untold-story-1111944/) 
3. [Ralph Himmelsbach spent the last 9 years of his FBI career on the 1971 D.B. Cooper hijacking, and then put it squarely behind him — at least, until someone else brought it up | The Seattle Times](https://www.seattletimes.com/pacific-nw-magazine/ralph-himmelsbach-spent-the-last-9-years-of-his-fbi-career-on-the-1971-d-b-cooper-hijacking-and-then-put-it-squarely-behind-him-at-least-until-someone-else-brought-it-up/) 
4. [Who Is DB Cooper? | Top 14 Theories | POPSUGAR Entertainment](https://www.popsugar.com/entertainment/who-is-db-cooper-48860250) 
5. [11 theories on the true identity of D.B. Cooper | Considerable](https://www.considerable.com/entertainment/history/who-was-db-cooper/) 
6. [Leading Theories About D.B. Cooper and 30 Other Unsolved Mysteries | Stacker](https://stacker.com/stories/3879/leading-theories-about-db-cooper-and-30-other-unsolved-mysteries) 
7. [50 years later, the mystery of D.B. Cooper still intrigues - OPB](https://www.opb.org/article/2021/11/24/d-b-cooper-50th-anniversary/) 
8. [D. B. Cooper - Wikipedia](https://en.wikipedia.org/wiki/D._B._Cooper) 
9. [D.B. Cooper Hijacking — FBI](https://www.fbi.gov/history/famous-cases/db-cooper-hijacking)
