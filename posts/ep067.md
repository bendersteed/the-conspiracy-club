title: Επεισόδιο 67: Μουσικές συνωμοσίες
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 67
date: 2022-04-12 00:30
tags: music, 2pac, avril, lavigne, hip-hop, stevie wonder
summary: Ο Αντώνης έρχεται στο σημερινό επεισόδιο για να συζητήσουμε διάφορες συνωμοσίες στο χώρο της pop μουσικής. Ζει ο 2Pac; Τι παίζει με την όραση του Stevie Wonder; Ποιο μυστήριο κρύβει η ζωή της Taylor Swift; Αυτά και ακόμα περισσότερα στο σημερινό επεισόδιο.
cover: https://theconspiracyclub.gr/assets/images/ep67.jpg
duration: "01:17:12"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep67.mp3" length:"37063999"
---

Ο Αντώνης έρχεται στο σημερινό επεισόδιο για να συζητήσουμε διάφορες συνωμοσίες στο χώρο της pop
μουσικής. Ζει ο 2Pac; Τι παίζει με την όραση του Stevie Wonder; Ποιο μυστήριο κρύβει η ζωή της
Taylor Swift; Αυτά και ακόμα περισσότερα στο σημερινό επεισόδιο.

## Σχετικά links:
1. [This bizarre conspiracy theory claims that Taylor Swift is the clone of a satanist priestess](https://www.digitalspy.com/showbiz/a807544/taylor-swift-weird-conspiracy-theory-satanist-clone-wait-wtf/)
2. [Here's why the theory that Taylor Swift is a satanist clone absolutely checks out](https://mashable.com/article/taylor-swift-satanist-clone-zeena-lavey)
3. [Is Taylor Swift a Clone Of Famous Satanist Zeena LaVey? No, Probably Not](https://www.nme.com/blogs/nme-blogs/taylor-swift-clone-of-a-famous-satanist-theory-3325)
4. [Kanye and Bowie: Music's Most WTF Conspiracy Theories, Explained - Rolling Stone](https://www.rollingstone.com/music/music-news/kanye-west-and-david-bowie-musics-most-wtf-conspiracy-theories-explained-196893/)
5. [Did David Bowie predict the rise of Kanye West? - BBC Reel](https://www.bbc.com/reel/video/p07bhkmn/did-david-bowie-predict-the-rise-of-kanye-west-)
6. [One Man Believes Tupac Is Alive in New Mexico, So He's Making a Movie - Rolling Stone](https://www.rollingstone.com/music/music-news/tupac-alive-movie-951140/)
7. [Why That 'Tupac Lives' Conspiracy Refuses to Die | Highsnobiety](https://www.highsnobiety.com/p/tupac-is-alive-theory/)
8. [Every Tupac Is Still Alive Conspiracy Theory Explained - Is Tupac Shakur Really Dead?](https://www.esquire.com/entertainment/music/a23595101/tupac-is-still-alive-conspiracy-theories/)
9. ["The Secret Meeting that Changed Rap Music and Destroyed a Generation" | Hip Hop Is Read](https://www.hiphopisread.com/2012/04/secret-meeting-that-changed-rap-music.html)
10. [Why fans think Avril Lavigne died and was replaced by a clone named Melissa | Celebrity | The Guardian](https://www.theguardian.com/lifeandstyle/shortcuts/2017/may/15/avril-lavigne-melissa-cloning-conspiracy-theories)
11. [Everyone’s favourite Avril Lavigne clone conspiracy theory is back | Dazed](https://www.dazeddigital.com/music/article/54108/1/the-avril-lavigne-clone-conspiracy-theory-is-back)
12. [stevie wonder is not blind: the truthers' case](https://deadspin.com/stevie-wonder-is-not-blind-the-evidence-1641795715)
13. [stevie wonder can see? a look at the wild rumor claiming the musician isn't really
    blind](https://www.suggest.com/stevie-wonder-can-see-rumor/466/)
14. [shaquille o'neal adds fuel conspiracy theory after claiming stevie wonder 'saw' him in an elevator - nz herald](https://www.nzherald.co.nz/sport/shaquille-oneal-adds-fuel-conspiracy-theory-after-claiming-stevie-wonder-saw-him-in-an-elevator/cmpowm2yhehohdro43jx2xx2la/)
15. [Impeccably Timed Rainbow at Grateful Dead Reunion Show Prompts Conspiracy Theories | Music News @ Ultimate-Guitar.Com](https://www.ultimate-guitar.com/news/general_music_news/impeccably_timed_rainbow_at_grateful_dead_reunion_show_prompts_conspiracy_theories.html)
