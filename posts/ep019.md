title: Επεισόδιο 19: Οι Σατανιστές της Παλλήνης
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 19
date: 2020-10-15 20:00
tags: cult, greek, politics
summary: Τι συνέβη στις αρχές του 90 και όλη η Ελλάδα κρυβόταν από Σατανιστές; Μια ιστορία για νέα παιδιά που αφέθηκαν στη βούληση των δαιμόνων και πως έφτασαν να διαπράξουν φρικτά εγκλήματα.
cover: https://theconspiracyclub.gr/assets/images/ep19.jpg
duration: "54:25"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep19.mp3" length:"26128086"
---

Τι συνέβη στις αρχές του 90 και όλη η Ελλάδα κρυβόταν από Σατανιστές; Μια
ιστορία για νέα παιδιά που αφέθηκαν στη βούληση των δαιμόνων και πως
έφτασαν να διαπράξουν φρικτά εγκλήματα.

## Σχετικά links:
1. [Οι σατανιστές της Παλλήνης -
   huffingtonpost.gr](https://www.huffingtonpost.gr/2016/12/24/eidhseis-koinwnia-nea-zwh-satanistes-pallhnhs_n_13838080.html)
2. [Η υπόθεση που έκανε την Ελλάδα να κοιμάται με ανοικτό το φως - oneman.gr](https://www.oneman.gr/longreads/satanistes-tis-pallinis-i-ipothesi-pou-ekane-tin-ellada-na-koimatai-me-anoixto-to-fos/)
3. [Οι σατανιστές της Παλλήνης -
   mixanitouxronou.gr](https://www.mixanitouxronou.gr/i-satanistes-tis-pallinis-ekanan-sexoualika-orgia-viasan-ke-skotosan-mia-28chroni-mitera-ke-mia-14chroni-kopela-sto-onoma-tou-satana/)
4. [Οι σατανιστές της Παλλήνης - athensvoice.gr](https://www.athensvoice.gr/greece/638010_oi-satanistes-tis-pallinis)
5. [Η μεγάλη και αιματηρή ιστορία των Ελλήνων σατανιστών](https://www.vice.com/el/article/pa3n9v/h-megalh-kai-aimathrh-istoria-twn-ellhnwn-satanistwn-ths-pallhnhs)
6. [Συνέντευξη Δημητροκάλη - youtube.com](https://www.youtube.com/watch?v=dZn-S1k7KPQ)
7. [Συνέντευξη Μαργέτη - youtube.com](https://www.youtube.com/watch?v=J4vCH1uyG7c)
