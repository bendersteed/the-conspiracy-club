title: Επεισόδιο 58: Βατικανό ΙΙ
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 58
date: 2022-01-24 23:30
tags: vatican, secret documents, holy grail, templar knights, ναΐτες
summary: Συνεχίζουμε το θέμα του Βατικανού, αυτή τη φορά με επίκεντρο στα μυστικά αρχεία που κρύβονται στη βιβλιοθήκη του. Ποια μυστικά μας αποκαλύπτουν οι πληροφορίες που θέλουν να αποκρύψουν από το κοινό; Τι κοινό μπορεί να έχει μια χρονομηχανή, οι απόγονοι του Ιησού και οι Ναΐτες ιππότες;
cover: https://theconspiracyclub.gr/assets/images/ep58.jpg
duration: "01:27:31"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep58.mp3" length:"42011637"
---
Συνεχίζουμε το θέμα του Βατικανού, αυτή τη φορά με επίκεντρο στα μυστικά αρχεία που κρύβονται στη
βιβλιοθήκη του. Ποια μυστικά μας αποκαλύπτουν οι πληροφορίες που θέλουν να αποκρύψουν από το κοινό;
Τι κοινό μπορεί να έχει μια χρονομηχανή, οι απόγονοι του Ιησού και οι Ναΐτες ιππότες;

## Σχετικά links:
1. [Ναΐτες Ιππότες - wikipedia.org](https://el.m.wikipedia.org/wiki/%CE%9D%CE%B1%CE%90%CF%84%CE%B5%CF%82_%CE%99%CF%80%CF%80%CF%8C%CF%84%CE%B5%CF%82)
2. [Ναΐτες Ιππότες - enimerotiko.gr](https://www.enimerotiko.gr/kosmos/na-tes-ippotes-o-mythos-toy-agioy-diskopotiroy-kai-i-mystiria-apomonomeni-ekklisia-me-ton-krymmeno-thisayro/amp/)
3. [Three Secrets of Fátima - wikipedia.org](https://en.m.wikipedia.org/wiki/Three_Secrets_of_F%C3%A1tima)
4. [Vatican Pornography Collection - snopes.com](https://www.snopes.com/fact-check/porn-again-christians/)
5. [Step Into the Vatican’s Secret Archives - history.com](https://www.history.com/.amp/news/step-into-the-vaticans-secret-archives)
6. [Vatican Apostolic Archive - wikipedia.org](https://en.m.wikipedia.org/wiki/Vatican_Apostolic_Archive)
