title: Επεισόδιο 66: Aleister Crowley
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 66
date: 2022-04-05 14:00
tags: occult, crowley, aleister, magick
summary: Ίσως ο πιο διάσημος εκφραστής του μυστικισμού στον 20ο αιώνα, ο άνθρωπος που κατηγορήθηκε ως ο πιο σατανικός άνθρωπος στον κόσμο και επηρέασε μεγάλο μέρος της pop κουλτούρας, φαίρνοντας τη μαγεία και τον μυστικισμό στο προσκήνιο. Ποιος ήταν ο αινιγματικός κ. Crowley και όλη η πολυτάραχη ζωή του, σε αυτό το επεισόδιο.
cover: https://theconspiracyclub.gr/assets/images/ep66.jpg
duration: "01:11:58"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep66.mp3" length:"34544763"
---

Ίσως ο πιο διάσημος εκφραστής του μυστικισμού στον 20ο αιώνα, ο άνθρωπος που κατηγορήθηκε ως ο πιο
σατανικός άνθρωπος στον κόσμο και επηρέασε μεγάλο μέρος της pop κουλτούρας, φαίρνοντας τη μαγεία και
τον μυστικισμό στο προσκήνιο. Ποιος ήταν ο αινιγματικός κ. Crowley και όλη η πολυτάραχη ζωή του, σε
αυτό το επεισόδιο.

## Σχετικά links:
1. [Aleister Crowley - Wikipedia](https://en.wikipedia.org/wiki/Aleister_Crowley#The_A%E2%88%B4A%E2%88%B4_and_The_Holy_Books_of_Thelema:_1907%E2%80%931909)
2. [Hermetic Order of the Golden Dawn - Wikipedia](https://en.wikipedia.org/wiki/Hermetic_Order_of_the_Golden_Dawn)
3. [Ordo Templi Orientis - Wikipedia](https://en.wikipedia.org/wiki/Ordo_Templi_Orientis)
4. [Thelema - Wikipedia](https://en.wikipedia.org/wiki/Thelema)
5. [Aeon (Thelema) - Wikipedia](https://en.wikipedia.org/wiki/Aeon_(Thelema))
6. [Abbey of Thelema - Wikipedia](https://en.wikipedia.org/wiki/Abbey_of_Thelema)
7. [Get To Know Aleister Crowley, The 'Wickedest Man In The World'](https://allthatsinteresting.com/aleister-crowley)
8. [The Great Beast 666: who was Aleister Crowley? | National Trust](https://www.nationaltrust.org.uk/features/the-great-beast-666-who-was-aleister-crowley)
9. [Aleister Crowley: The Wickedest Man in the World Documents the Life of the Bizarre Occultist,
   Poet & Mountaineer | Open Culture](https://www.openculture.com/2014/03/aleister-crowley-the-wickedest-man-in-the-world.html)
10. [Masters of Darkness - the Wickedest Man in the World (part 1) - YouTube](https://www.youtube.com/watch?v=rSdXFO9n2-k)
11. [Aleister Crowley- The Most Wicked Man In The World - Documentary - YouTube](https://www.youtube.com/watch?v=jiBwPbhAfF4)
