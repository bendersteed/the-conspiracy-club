title: Επεισόδιο 57: Παιδιά Indigo
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 57
date: 2022-01-22 15:30
tags: indigo, παιδιά του 83, psychic, new world
summary: Ένα θέμα που ήταν πολύ δημοφιλές τη δεκαετία του 90, τα Παιδιά Indigo έχουν έρθει για να ανοίξουν νέους δρόμους στην ανθρώπινη ιστορία. Πού βρίσκονται σήμερα και ποιοι είναι οι διάδοχοί τους; 
cover: https://theconspiracyclub.gr/assets/images/ep57.jpg
duration: "56:35"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep57.mp3" length:"27164194"
---
Ένα θέμα που ήταν πολύ δημοφιλές τη δεκαετία του 90, τα Παιδιά Indigo έχουν έρθει για να ανοίξουν
νέους δρόμους στην ανθρώπινη ιστορία. Πού βρίσκονται σήμερα και ποιοι είναι οι διάδοχοί τους;

## Σχετικά links:
1. [Kirlian photography - Wikipedia](https://en.wikipedia.org/wiki/Kirlian_photography)
2. [Aura (paranormal) - Wikipedia](https://en.wikipedia.org/wiki/Aura_(paranormal))
3. [Inside the Strange, Psychic World of Indigo Children - YouTube](https://www.youtube.com/watch?v=SL5Rd3Bnxms)
4. [Παιδιά ίντιγκο, κρύσταλλα και ουράνια τόξα | Νοόσφαιρα Wellness](https://www.noosfera.gr/pedia-intigko-krystalla-ke-ourania-toxa/])
5. [KLIK Magazine | Indigo | Μήπως ανήκετε κι εσείς σε μια από τις 5 γενιές των Φωτισμένων Παιδιών;](https://www.klik.gr/gr/el/blender/indigo-mipos-anikete-ki-eseis-se-mia-apo-tis-5-genies-ton-fotismenon-paidion/])
6. [Τα φωτισμένα Παιδιά Indigo ή αλλιώς Παιδιά του 83 | iEllada.gr](https://www.iellada.gr/kosmos/ta-fotismena-paidia-indigo-i-allios-paidia-toy-83])
7. [Nancy Ann Tappe - What on earth are life colors?](http://www.nancyanntappe.com/what_on_earth_are_life_colors])
8. [Home](https://web.archive.org/web/20090206083041/http:/www.allaboutindigos.com/])
9. [Edgar Cayce - Wikipedia](https://en.wikipedia.org/wiki/Edgar_Cayce])
10. [What's an Indigo Child? What You Need to Know About This New Age Term](https://www.scarymommy.com/indigo-children/])
11. [Indigo children - Wikipedia](https://en.wikipedia.org/wiki/Indigo_children])
12. [So-Called Indigo Teen Says She Can Read People - ABC News](https://abcnews.go.com/GMA/AmericanFamily/story?id=2224795&page=1])
13. [Are They Here to Save the World? - The New York Times](https://www.nytimes.com/2006/01/12/fashion/thursdaystyles/are-they-here-to-save-the-world.html])
14. [The chosen ones | Society | The Guardian](https://www.theguardian.com/society/2006/aug/05/familyandrelationships.lifeandhealth])
15. [17 Signs That You Are An Indigo Child | by Francesca Dallaglio | Know Thyself, Heal Thyself | Medium](https://medium.com/know-thyself-heal-thyself/17-signs-that-you-are-an-indigo-child-13ba21df0016])
16. [Test για να δεις αν είσαι παιδί Indigo](https://lonerwolf.com/indigo-child-test/)
