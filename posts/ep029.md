title: Επεισόδιο 29: Το τέλος του κόσμου
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 29
date: 2021-01-18 16:00
tags: 2012, end, world
summary: Καθώς το 2012 πλησίαζε στο τέλος του πολλοί ήταν εκείνοι που περίμεναν και το τέλος του κόσμου. Τελικά όμως φάνηκε πως ήταν έτσι τα πράγματα. Ή μήπως όχι; Ήρθε το τέλος σε κάποια στιγμή στο παρελθόν και τι ρόλο παίζει το Cern σε αυτή την ιστορία;
cover: https://theconspiracyclub.gr/assets/images/ep29.jpg
duration: "00:54:06"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep29.mp3" length:"25972563"
---

Καθώς το 2012 πλησίαζε στο τέλος του πολλοί ήταν εκείνοι που περίμεναν και το τέλος του
κόσμου. Τελικά όμως φάνηκε πως ήταν έτσι τα πράγματα. Ή μήπως όχι; Ήρθε το τέλος σε κάποια στιγμή
στο παρελθόν και τι ρόλο παίζει το Cern σε αυτή την ιστορία;

## Σχετικά links:
1. [Sagittarius A* - wikipedia.org](https://en.wikipedia.org/wiki/Sagittarius_A*)
2. [Shiva Hypothesis - wikipedia.org](https://en.wikipedia.org/wiki/Shiva_Hypothesis)
3. [Nick Hintonn's twitter thread - twitter.com](https://twitter.com/NickHintonn/status/1154523206692331520)
4. [4chan thread - randomarchive.com](https://randomarchive.com/board/b/thread/700602767/i-am-one-of-23-scientists-responsible-for-what-you-call-the-mandela)
5. [The world ended in 2012 - medium.com](https://medium.com/conspiracy-custard/the-world-ended-in-2012-ca344c629462)
6. [The Montauk Project - wikipedia.org](https://en.wikipedia.org/wiki/The_Montauk_Project:_Experiments_in_Time)
7. [Does the Mandela effect prove the world ended in 2012 - quora.com](https://www.quora.com/Does-the-Mandela-effect-prove-the-world-ended-in-2012)
[A Theory Went Viral On Twitter Saying That We All Died In 2012 And Have Been Living In A Simulation Ever Since - barstoolsports.com](https://www.barstoolsports.com/blog/1377635/a-theory-went-viral-on-twitter-saying-that-we-all-died-in-2012-and-have-been-living-in-a-simulation-ever-since)
8. [Mandela Effect The World Did End In 2012 - youtube.com](https://www.youtube.com/watch?v=pH3exI6I1iY)
9. [Επιστροφή στα 80’s : Έμπνευση από το παρελθόν ή εμπόριο νοσταλγίας; - spoilerarlert.gr](https://www.spoileralert.gr/back-to-the-80s/)
10. [Illusions of Time - youtube.com](https://www.youtube.com/watch?v=zHL9GP_B30E)
