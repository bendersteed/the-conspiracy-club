title: Επεισόδιο 91: Καλικάντζαροι
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 91
date: 2022-12-26 16:30
tags: christmas, goblins, folk
summary: Την περίοδο που διανύουμε βγαίνουν στην επιφάνεια της γης κάποια μυστήρια πλάσματα που πολλοί τα αποκαλούν καλικάντζαρους. Ποιες οι δοξασίες γύρω από τους καλικάντζαρους και ποιοι οι τρόποι για να προστατευτούμε;
cover: https://theconspiracyclub.gr/assets/images/ep91.jpg
duration: "01:04:27"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep91.mp3" length:"30936331"
---
Την περίοδο που διανύουμε βγαίνουν στην επιφάνεια της γης κάποια
μυστήρια πλάσματα που πολλοί τα αποκαλούν καλικάντζαρους. Ποιες οι
δοξασίες γύρω από τους καλικάντζαρους και ποιοι οι τρόποι για να
προστατευτούμε;

## Σχετικά links:
1. [Καλικάντζαρος - Βικιπαίδεια](https://el.wikipedia.org/wiki/%CE%9A%CE%B1%CE%BB%CE%B9%CE%BA%CE%AC%CE%BD%CF%84%CE%B6%CE%B1%CF%81%CE%BF%CF%82)
2. [Έθιμα Χριστουγέννων: Τα Τσιλικρωτά στη Δυτική Μάνη - ΕΚΚΛΗΣΙΑ ONLINE](https://www.ekklisiaonline.gr/nea/ethima-christougennon-ta-tsilikrota-sti-dytiki-mani/)
3. [Καλικάντζαροι : Ολα όσα πρέπει να γνωρίζετε για τα τερατάκια των Χριστουγέννων | in.gr](https://www.in.gr/2021/01/06/life/stories/kalikantzaroi-ola-osa-prepei-na-gnorizete-gia-ta-teratakia-ton-xristougennon/)
4. [Καλικάντζαροι. Πότε εμφανίζονται & πως να προστατευτείτε](https://www.helppost.gr/xristougenna/ethima-xristougennon/kalikantzaroi/)
5. [10 μύθοι από όλη την Ελλάδα για τους Καλικάντζαρους, που βγαίνουν τα Χριστούγεννα από τη γη για να πειράξουν τους ανθρώπους - Οι 18 τύποι καλικάντζαρων | Βίντεο - Αγώνας της ΚρήτηςΑγώνας της Κρήτης](https://agonaskritis.gr/%CE%B7-%CE%BB%CE%AD%CE%BE%CE%B7-%CF%84%CE%B7%CF%82-%CE%B5%CE%B2%CE%B4%CE%BF%CE%BC%CE%AC%CE%B4%CE%B1%CF%82-%CE%BA%CE%B1%CE%BB%CE%B9%CE%BA%CE%AC%CE%BD%CF%84%CE%B6%CE%B1%CF%81%CE%BF%CF%82/)
6. [Οι καλικάντζαροι στην Ελλάδα και σε όλο τον Κόσμο – kolydas.eu](https://kolydas.eu/2022/01/05/%CE%BF%CE%B9-%CE%BA%CE%B1%CE%BB%CE%B9%CE%BA%CE%AC%CE%BD%CF%84%CE%B6%CE%B1%CF%81%CE%BF%CE%B9/)
7. [Οι καλικάντζαροι και η ιστορία τους | Πολιτισμός](https://www.maxmag.gr/politismos/kalikantzari-ke-istoria-tous/)
8. [Οι "καλικάντζαροι" προκάλεσαν και φέτος ζημιές στο Τυχερό Έβρου!(φωτο) - e-evros.gr](https://www.e-evros.gr/gr/eidhseis/3/oi-kalikantzaroi-prokalesan-kai-fetos-zhmies-sto-tyxero-ebroy-fwto/post36741)
9. [Ξωτικά, παγανά και καλικάντζαροι | Εφημερίδα Πρωινή](https://proini.news/xotika-panaga-kai-kalikantzaroi/)
10. [Οι καλικάντζαροι στη λαϊκή παράδοση | ΑΡΓΟΛΙΚΗ ΑΡΧΕΙΑΚΗ ΒΙΒΛΙΟΘΗΚΗ ΙΣΤΟΡΙΑΣ ΚΑΙ ΠΟΛΙΤΙΣΜΟΥ](https://argolikivivliothiki.gr/2016/01/04/the-goblins-in-folk-tradition/)
