title: Επεισόδιο 78: Ερπετόμορφοι
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 78
date: 2022-09-19 14:00
tags: reptilians, queen, icke
summary: Στο σημερινό επεισόδιο, ανοίγουμε ξανά το θέμα των ερπετόμορφων. Ποια η επιρροή τους στην ανθρωπότητα και ποια η μυστική ατζέντα τους; Ποια η σχέση τους με τη βασιλική οικογένεια της Αγγλίας;
cover: https://theconspiracyclub.gr/assets/images/ep78.jpg
duration: "01:08:00"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep78.mp3" length:"32642375"
---
Στο σημερινό επεισόδιο, ανοίγουμε ξανά το θέμα των ερπετόμορφων. Ποια η επιρροή τους στην ανθρωπότητα και ποια
η μυστική ατζέντα τους; Ποια η σχέση τους με τη βασιλική οικογένεια της Αγγλίας;

## Σχετικά links:
1. [Reptilian conspiracy theory - wikipedia.org](https://en.wikipedia.org/wiki/Reptilian_conspiracy_theory)
2. [David Icke - wikipedia.org](https://en.wikipedia.org/wiki/David_Icke#Self-publishing)
3. [Is Queen Elizabeth an extraterrestrial reptile? - news24.com](https://www.news24.com/channel/gossip/royal-news/is-queen-elizabeth-an-extraterrestrial-reptile-some-of-the-most-bizarre-royal-conspiracy-theories-20200615)
4. [The Bush Family Genealogy - educate-yourself.org](https://educate-yourself.org/cn/bushbloodline.shtml)
5. [Merovingian dynasty - wikipedia.org](https://en.wikipedia.org/wiki/Merovingian_dynasty)
