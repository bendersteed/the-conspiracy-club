title: Επεισόδιο 15: Κορωνοϊός - Μετά την καραντίνα
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 15
date: 2020-06-02 21:00
tags: coronavirus, reality, conspiracy
summary: Η πανδημία που συγκλονίζει τον κόσμο αυτή τη στιγμή. Τι κρύβεται πίσω από το μεγαλύτερο συμβάν του 2020; Οι σκέψεις που απασχολούν την ανθρωπότητα σε αυτό το επεισόδιο.
cover: https://theconspiracyclub.gr/assets/images/ep15.jpg
duration: "01:12:59"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep15.mp3" length:"55164498"
---

Η πανδημία που συγκλονίζει τον κόσμο αυτή τη στιγμή. Τι κρύβεται πίσω
από το μεγαλύτερο συμβάν του 2020; Οι σκέψεις που απασχολούν την
ανθρωπότητα σε αυτό το επεισόδιο.

## Σχετικά links:
1. [Misinformation related to the COVID-19 pandemic - wikipedia.org](https://en.wikipedia.org/wiki/Misinformation_related_to_the_COVID-19_pandemic#Conspiracy_theories)
2. [Opinion: Coronavirus conspiracy theories on the rise - dw.com](https://www.dw.com/en/opinion-coronavirus-conspiracy-theories-on-the-rise/a-53418223)
3. [What is the truth behind the 5G coronavirus conspiracy theory? - euronews.com](https://www.euronews.com/2020/05/15/what-is-the-truth-behind-the-5g-coronavirus-conspiracy-theory-culture-clash)
4. [Την ύποπτη σύνδεση του Μπιλ Γκέιτς με την πανδημία αποκάλυψε ο Σταμάτης Γονίδης - luben.tv](https://luben.tv/stream/197469)
5. [Πρωτοσέλιδο της Ελεύθερης Ώρας - frontpages.gr](https://www.frontpages.gr/data/2019/20190622/EloraI.jpg)



