title: Επεισόδιο 38: 1000 χρόνια Conspiracy Club
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 38
date: 2021-03-23 14:00
tags: birthday, rf, antennas
summary: Σε αυτό το επετειακό επεισόδιο, συζητάμε για την ιστορία του podcast, τι γουστάρουμε στις συνωμοσίες και πως βλέπουμε τα πράγματα μετά από ένα χρόνο ύπαρξης. Μπόνους θέμα με μυστηριώδεις αναμεταδότες στην Κέρκυρα!
cover: https://theconspiracyclub.gr/assets/images/ep38.jpg
duration: "57:24"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep38.mp3" length:"27561942"
---

Σε αυτό το επετειακό επεισόδιο, συζητάμε για την ιστορία του podcast, τι γουστάρουμε στις συνωμοσίες
και πως βλέπουμε τα πράγματα μετά από ένα χρόνο ύπαρξης. Μπόνους θέμα με μυστηριώδεις αναμεταδότες
στην Κέρκυρα!

## Σχετικά links:
1. [My Radiation Overdose in Corfu Greece - medium.com](https://medium.com/@_ken/my-radiation-overdose-in-corfu-greece-d09455b552db)
