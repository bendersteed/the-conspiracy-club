title: Επεισόδιο 25: Ζει ο Paul McCartney;
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 25
date: 2020-11-23 19:00
tags: beatles, music, mccartney
summary: Τελικά το "Hey Jude" το έγραψε ο Paul McCartney; Ποια μυστικά μηνύματα υπάρχουν στα τραγούδια των Beatles για τον πραγματικό Paul McCartney; Ο Ρολάνδος έρχεται σε αυτό το επεισόδιο για να μας μιλήσει για μια από τις παλαιότερες συνωμοσίες της μουσικής βιομηχανίας.
cover: https://theconspiracyclub.gr/assets/images/ep25.jpg
duration: "56:24"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep25.mp3" length:"27076372"
---

Τελικά το "Hey Jude" το έγραψε ο Paul McCartney; Ποια μυστικά μηνύματα υπάρχουν στα τραγούδια των
Beatles για τον πραγματικό Paul McCartney; Ο Ρολάνδος έρχεται σε αυτό το επεισόδιο για να μας
μιλήσει για μια από τις παλαιότερες συνωμοσίες της μουσικής βιομηχανίας.

## Σχετικά links:
[Paul is dead - wikipedia.org](https://en.wikipedia.org/wiki/Paul_is_dead)
[Michingan Daily archives 14-10-69 - digital.bentley.umich.edu](https://digital.bentley.umich.edu/midaily/mdp.39015071754159/374)
[WKBW: Paul McCartney is alive and well - maybe - reelradio.com](http://www.reelradio.com/gifts/pmwkbw69.html)
[‘Paul Is Dead’: The bizarre story of music’s most notorious conspiracy theory - rollingstone.com  ](https://www.rollingstone.com/music/music-features/paul-mccartney-is-dead-conspiracy-897189/)
[Proof Paul McCartney died or something else? - youtube.com](https://www.youtube.com/watch?v=0NF64gSBhus)
[Paul is dead - time.com](http://content.time.com/time/specials/packages/article/0,28804,1860871_1860876_1860997,00.html)
[10 key elements to the paul mccartney is dead theory - whatculture.com](https://whatculture.com/music/10-key-elements-to-the-paul-mccartney-is-dead-theory)
[The Beatles never broke up](http://www.thebeatlesneverbrokeup.com/)
