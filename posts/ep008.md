title: Επεισόδιο 8: Επίπεδη Γη
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 8
date: 2020-04-19 18:00
tags: earth, science, conspiracy
summary: Ο Κωνσταντίνος αναλύει σε βάθος την κοινότητα γύρω από τις θεωρίες της Επίπεδης Γης. Κυνηγοί της αλήθειας που ρισκάρουν μέχρι και τη ζωή τους, χελώνες, η NASA και όλες οι θεωρίες για το σχήμα του πλανήτη μας.
cover: https://theconspiracyclub.gr/assets/images/ep08.jpg
duration: "01:06:11"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep08.mp3" length:"63537451"
---

Ο Κωνσταντίνος αναλύει σε βάθος την κοινότητα γύρω από τις θεωρίες της
Επίπεδης Γης. Κυνηγοί της αλήθειας που ρισκάρουν μέχρι και τη ζωή
τους, χελώνες, η NASA και όλες οι θεωρίες για το σχήμα του πλανήτη
μας.


## Σχετικά links:
1. [Flat-Earthers are back: 'It’s almost like the beginning of a new religion' - theguardian.com](https://www.theguardian.com/science/2016/jan/20/flat-earth-believers-youtube-videos-conspiracy-theorists)
2. [Modern flat Earth societies - wikipedia.org](https://en.wikipedia.org/wiki/Modern_flat_Earth_societies)
3. [Flat Earth: A Measured Response - youtube.com](https://www.youtube.com/watch?v=2gFsOoKAHZg)
4. [DIY Rockets, Daredevils, and the Tragedy of Mad Mike Hughes - wired.com](https://www.wired.com/story/diy-rockets-tragedy-mad-mike-hughes/)
5. [He’s semi-famous for being flat-out wrong about Earth - heraldnet.com](https://www.heraldnet.com/news/hes-semi-famous-for-being-flat-out-wrong-about-earth/)
6. [FLAT EARTH CLUES  - youtube.com](https://www.youtube.com/watch?v=T8-YdgU-CF4&list=PLltxIX4B8_URNUzDE2sXctnUAEXgEDDGn&index=2)
