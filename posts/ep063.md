title: Επεισόδιο 63: God's Ego Death, God's Last Wish
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 63
date: 2022-03-08 21:35
tags: god, ego-death, last wish, iceberg
summary: Δυο φράσεις που τελευταία απαντώνται συχνά στους συνομωσιολογικούς κύκλους. Τι σημαίνουν αυτές οι κρυπτικές φράσεις, τι μυστήρια κρύβουν και τι έχουν να κάνουν με τις πάντα ενδιαφέρουσες θεολογικές αναζητήσεις του ανθρώπου;
cover: https://theconspiracyclub.gr/assets/images/ep63.jpg
duration: "00:50:34"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep63.mp3" length:"24274211"
---
Δυο φράσεις που τελευταία απαντώνται συχνά στους συνομωσιολογικούς κύκλους. Τι σημαίνουν αυτές οι
κρυπτικές φράσεις, τι μυστήρια κρύβουν και τι έχουν να κάνουν με τις πάντα ενδιαφέρουσες θεολογικές
αναζητήσεις του ανθρώπου;

Το θέμα αυτό σχεδόν συζητήθηκε με τους φίλους [CrimeGroupers](https://open.spotify.com/show/7a2ErveU0FEpgUpMvA8a96), που κάνουν ένα πολύ ενδιαφέρον podcast για εγκλήματα!
## Σχετικά links:
1. [Limitless Possibilities: Megalist: An Introduction and Summary of 500 Conspiracy Theories](https://www.vertigo22.com/2020/02/megalist-introduction-and-summary-of.html)
2. [/x/ - God's Last Wish - Paranormal - 4chan](http://web.archive.org/web/20190705132518/https:/yuki.la/x/20340895)
3. [/x/ - Gods ego death What does it mean??? - Paranormal - 4chan](http://web.archive.org/web/20190705140338/https:/yuki.la/x/19064268)
4. [/x/ - Gods last Wish - Paranormal - 4chan](http://web.archive.org/web/20190705134019/https:/yuki.la/x/20250083)
5. [God’s Last Wish - YouTube](https://www.youtube.com/watch?v=LxcQpWhlcig)
6. [Atheism, Evidence, and the “God-of-the-Gaps” : Strange Notions](https://strangenotions.com/god-of-the-gaps/)
7. [God of the gaps - Wikipedia](https://en.wikipedia.org/wiki/God_of_the_gaps)
8. [Ego death - Wikipedia](https://en.wikipedia.org/wiki/Ego_death)
9. [The God Complex: The Death of A God Ego In Jesus' Crucifixion and Resolution of Divine Identity Crisis - Healing Religious Trauma, Deconstruction of Evangelical Christianity](https://lifeafterdogma.org/2019/09/02/the-god-complex/)
10. [The Unspeakable Conspiracy: God Was Raped - YouTube](https://www.youtube.com/watch?v=k-xtlMqncyM)
11. [i was studying some texts the other day and came across the term “god’s ego death”. I was wondering if anyone had any information or links pertaining to it as i couldn’t find anything on the surface web, and i’m really interested in finding out what its about. : PhilosophyofReligion](https://www.reddit.com/r/PhilosophyofReligion/comments/mzk32c/i_was_studying_some_texts_the_other_day_and_came/)
12. [Gods ego death? Theories and explanations welcome. : wendigoon](https://www.reddit.com/r/wendigoon/comments/lbhkg3/gods_ego_death_theories_and_explanations_welcome/)
13. [Possible meanings of Tier 0 theories "All Conspiracies are True", "God's Ego Death" and "God's Last Wish". : wendigoon](https://www.reddit.com/r/wendigoon/comments/rjg7xh/possible_meanings_of_tier_0_theories_all/)
14. [What if God got bored - Alan Watts - YouTube](https://www.youtube.com/watch?v=ckiNNgfMKcQ)
15. [Was It Jesus Christ Or Judas That Was Crucified At Calvary?](http://wallingtongospelhall.org/was-it-jesus-christ-or-judas-that-was-crucified-at-calvary/)

 
