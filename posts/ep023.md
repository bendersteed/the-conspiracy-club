title: Επεισόδιο 23: Προσελήνωση του Apollo 11
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 23
date: 2020-11-08 13:00
tags: moon, landing, nasa
summary: Τελικά ο άνθρωπος έχει πατήσει στο φεγγάρι; Ή μήπως το διάσημο βίντεο από την αποστολή του Apollo 11 είναι μια καλοστημένη απάτη; Τι σχέση έχουν ένας διάσημος σκηνοθέτης και η NASA; Όλες οι θεωρίες για μια από τις στιγμές που άλλαξαν για πάντα την ιστορία.
cover: https://theconspiracyclub.gr/assets/images/ep23.jpg
duration: "01:07:23"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep23.mp3" length:"32347912"
---

Τελικά ο άνθρωπος έχει πατήσει στο φεγγάρι; Ή μήπως το διάσημο βίντεο από την αποστολή του Apollo 11
είναι μια καλοστημένη απάτη. Τι σχέση έχουν ένας διάσημος σκηνοθέτης και η NASA; Όλες οι θεωρίες για
μια από τις στιγμές που άλλαξαν για πάντα την ιστορία.

## Σχετικά links:
1. [Space Race - wikipedia.org](https://en.wikipedia.org/wiki/Space_Race)
2. [List of Apollo missions - wikipedia.org](https://en.wikipedia.org/wiki/List_of_Apollo_missions)
3. [One giant lie - theguardian.com](https://www.theguardian.com/science/2019/jul/10/one-giant-lie-why-so-many-people-still-think-the-moon-landings-were-faked)
4. [Moon landing conspiracy theories aren't true - here's how we know - bbc.co.uk](https://www.bbc.co.uk/newsround/48774080)
5. [Moon-Landing Hoax Still Lives On, 50 Years After Apollo 11 - space.com](https://www.space.com/apollo-11-moon-landing-hoax-believers.html)
6. [Apoll 11 gallery](https://www.nasa.gov/apollo11-gallery)
7. [Buzz Aldrin punches denier - youtube.com](https://www.youtube.com/watch?v=7Y-Pc0cz-9o)
