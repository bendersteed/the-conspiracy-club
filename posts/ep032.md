title: Επεισόδιο 32: Τα μυστικά του Ομπάμα
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 32
date: 2021-02-10 18:00
tags: usa, potus, obama,
summary: Παρότι ένας από τους πιο δημοφιλής προέδρους της Αμερικής, ο Barack Obama βρίσκεται συχνά στο επίκεντρο συχνών συνωμοσιών. Ποια τα σκοτεινά στοιχεία της ζωής του; 
cover: https://theconspiracyclub.gr/assets/images/ep32.jpg
duration: "55:48"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep32.mp3" length:"26788875"
---

Παρότι ένας από τους πιο δημοφιλής προέδρους της Αμερικής, ο Barack Obama βρίσκεται συχνά στο
επίκεντρο συχνών συνωμοσιών. Ποια τα σκοτεινά στοιχεία της ζωής του;

## Σχετικά links:
1. [No, Michelle Obama is not a transgender woman - politifact.com](https://www.politifact.com/factchecks/2020/oct/31/facebook-posts/no-michelle-obama-not-transgender-woman/)
2. [Trump’s treasury pick said obama was a muslim who took orders from terrorists - vanityfair.com](https://www.vanityfair.com/news/2019/07/monica-crowley-barack-obama)
3. [Obama's alien bodyguard - weeklytimesnow.com](https://www.weeklytimesnow.com.au/news/obamas-alien-bodyguard/video/5020e643dadfff715dddaa81d064a519)
4. [The Obama conspiracy theories just keep coming - chicagotribune.com](https://www.chicagotribune.com/columns/clarence-page/ct-perspec-page-obama-conspiracy-theories-20171013-story.html)
5. [One in four Americans think Obama may be the antichrist, survey says - theguardian.com](https://www.theguardian.com/world/2013/apr/02/americans-obama-anti-christ-conspiracy-theories)
6. [The un-dead Obama birth certificate story - ussc.edu.au](https://www.ussc.edu.au/analysis/the-un-dead-obama-birth-certificate-story)
