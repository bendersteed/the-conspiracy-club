title: Επεισόδιο 17: Heaven's Gate
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 17
date: 2020-09-21 20:00
tags: history, reality, conspiracy, cult
summary: Μια διαστημική-θρησκευτικη αίρεση, που μετά από ιστορία χρόνων κατέληξε με μια μεγάλη έξοδο. Πώς δημιουργήθηκε και τι πίστευαν τα μέλη μιας από τις πρόσφατες και διαβόητες αιρέσεις.
cover: https://theconspiracyclub.gr/assets/images/ep17.jpg
duration: "01:00:38"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep17.mp3" length:"29105559"
---

Ή οι Πύλες του Ουρανού: μια διαστημική-θρησκευτικη αίρεση, που μετά από
ιστορία χρόνων κατέληξε με μια μεγάλη έξοδο. Πώς δημιουργήθηκε και τι
πίστευαν τα μέλη μιας από τις πρόσφατες και διαβόητες αιρέσεις.

## Σχετικά links:
1. [What the Heaven's Gate suicides say about American culture - theconversation.com](https://theconversation.com/amp/what-the-heavens-gate-suicides-say-about-american-culture-74343)
2. [Heaven's Gate - The End](https://academic.oup.com/jcmc/article/3/3/JCMC334/4584381)
3. [10 things you didn't know about Heaven's Gate - rollingstone.com](https://www.rollingstone.com/feature/heavens-gate-20-years-later-10-things-you-didnt-know-114563/amp/)
4. [Heaven's Gate members found dead - history.com](https://www.history.com/.amp/this-day-in-history/heavens-gate-cult-members-found-dead)
5. [Heaven's Gate - wikipedia.org](https://en.m.wikipedia.org/wiki/Heaven%27s_Gate_(religious_group))
6. [How and When It May Be Entered](https://www.heavensgate.com/)
