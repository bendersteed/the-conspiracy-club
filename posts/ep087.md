title: Επεισόδιο 87: Ο Αρχάνθρωπος των Πετραλώνων
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 87
date: 2022-11-29 02:00
tags: antropology, poulianos, protsch, petralona, chalkidiki
summary: Ένα σπήλαιο ανακαλύπτεται τυχαία και εντός του ένα κρανίο που φέρνει νέα ευρήματα της ανθρωπολογίας. Μια σειρα παρακωλύσεων αφήνουν πολλά ερωτηματικά για το αν κάποιοι ενοχλούνται από τα ευρήματα. Όλα τα σενάρια για τον Αρχάνθρωπο των Πετραλώνων, στο σημερινό επεισόδιο.
cover: https://theconspiracyclub.gr/assets/images/ep87.jpg
duration: "01:04:41"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep87.mp3" length:"31048305"
---
Ένα σπήλαιο ανακαλύπτεται τυχαία και εντός του ένα κρανίο που φέρνει
νέα ευρήματα της ανθρωπολογίας. Μια σειρα παρακωλύσεων αφήνουν πολλά
ερωτηματικά για το αν κάποιοι ενοχλούνται από τα ευρήματα. Όλα τα
σενάρια για τον Αρχάνθρωπο των Πετραλώνων, στο σημερινό επεισόδιο.

## Σχετικά links:
1. [Aris Poulianos - Wikipedia](https://en.wikipedia.org/wiki/Aris_Poulianos)
2. [Petralona Cave - Wikipedia](https://en.wikipedia.org/wiki/Petralona_Cave#Petralona_skull)
3. [Evolution of Modern Humans: Homo heidelbergensis](https://www2.palomar.edu/anthro/homo2/mod_homo_1.htm)
4. [The significance of the fossil hominid skull from Petralona, Greece - ScienceDirect](https://www.sciencedirect.com/science/article/abs/pii/0305440379900025)
5. [aee index gr](http://www.aee.gr/hellenic/1contents/contents.html)
6. [The human skull that challenges the Out of Africa theory | Ancient Origins](https://www.ancient-origins.net/human-origins-science/human-skull-challenges-out-africa-theory-001283)
7. [New Information on the Petralona Skull Controversy | Ancient Origins](https://www.ancient-origins.net/news-history-archaeology-opinion-guest-authors/new-information-petralona-skull-controversy-001380)
8. [The human skull that challenges the Out of Africa theory - Hellenic Daily News](https://www.hellenicdailynewsny.com/en-us/good-living/art-cultureus/the-human-skull-that-challenges-the-out-of-africa-theory)
9. [Cave in Northern Greece Is Yielding Evidence of Early Human Life - The New York Times](https://www.nytimes.com/1977/06/22/archives/cave-in-northern-greece-is-yielding-evidence-of-early-human-life.html)
10. [1. Valid, Fake and False Homo Theories – George Saos](https://georgesaos.com/category/1-valid-fake-and-false-homo-theories/)
11. [Prehistoric "Human Skull" Petralona Cave in Greece Reopening Soon](https://greekreporter.com/2022/02/16/greece-petralona-cave-reopening-prehistoric/)
12. [Η αμφισβητούμενη θεωρία ότι ο πρώτος Homo Sapiens ήταν ο Αρχάνθρωπος της Χαλκιδικής. Το ανθρώπινο κρανίο και η κόντρα των αρχαιολόγων - ΜΗΧΑΝΗ ΤΟΥ ΧΡΟΝΟΥ](https://www.mixanitouxronou.gr/i-katarripsi-tis-theorias-oti-o-protos-homo-sapiens-itan-o-archanthropos-tis-chalkidikis/)
13. ["Ο Αρχάνθρωπος των Πετραλώνων": Το σπουδαίο ντοκιμαντέρ που "κόπηκε" από την κρατική τηλεόραση (βίντεο) - Αργολικές Ειδήσεις](https://www.argolikeseidhseis.gr/2017/12/blog-post_21.html)
14. [Άρης Πουλιανός: Ό,τι δεν θέλουν να ξέρουμε για το σπήλαιο των Πετραλώνων | Heptapolis](https://heptapolis.com/el/aris-poylianos-oti-den-theloyn-na-xeroyme-gia-spilaio-ton-petralonon)
15. [“Ο Αρχάνθρωπος των Πετραλώνων”: H λογοκριμένη ταινία της ΥΕΝΕΔ μετά από 40 χρόνια](https://www.lavart.gr/o-archanthropos-ton-petralonon-h-logokrimeni-tainia-tis-yened-meta-apo-40-chronia/)
16. [The human skull that challenges the Out of Africa theory | Ancient Origins](https://www.ancient-origins.net/human-origins-science/human-skull-challenges-out-africa-theory-001283)
17. [Ο Αρχάνθρωπος των Πετραλώνων "ενόχλησε", διότι τοποθετεί τεκμηριωμένα τον άνθρωπο έξω από την Αφρική;](https://kartsonakis.blogspot.com/2021/09/blog-post_35.html?m=1&fbclid=IwAR3vF9uodurboLkRH0o1kxttkO7DcOPIl3ZMeF_kdqSPBDOpMG3KfvJ5_tE)
