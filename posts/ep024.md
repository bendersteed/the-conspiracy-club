title: Επεισόδιο 24: UFOs στο Roswell
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 24
date: 2020-11-16 13:00
tags: ufo, roswell, aliens
summary: Μας έχουν τελικά επισκεφτεί όντα από άλλο πλανήτη; Σε αυτό το επεισόδιο συζητάμε για το ιστορικό και τις θεωρίες για τη φημολογούμενη συντριβή εξωγήινου διαστημοπλοίου στο Roswell. Άλλο ένα κομμάτι του παζλ στην αναζήτηση της εξωγήινης ζωής.
cover: https://theconspiracyclub.gr/assets/images/ep24.jpg
duration: "01:15:02"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep24.mp3" length:"36022031"
---

Μας έχουν τελικά επισκεφτεί όντα από άλλο πλανήτη; Σε αυτό το επεισόδιο συζητάμε για το ιστορικό και
τις θεωρίες για τη φημολογούμενη συντριβή εξωγήινου διαστημοπλοίου στο Roswell. Άλλο ένα κομμάτι του
παζλ στην αναζήτηση της εξωγήινης ζωής.

## Σχετικά links:
1. [Roswell UFO incident - wikipedia.org](https://en.wikipedia.org/wiki/Roswell_UFO_incident#Friedman's_initial_work)
2. [Alien autopsy - wikipedia.org](https://en.wikipedia.org/wiki/Alien_autopsy)
3. [Roswell UFO crash: what really happened 67 years ago? - theweek.co.uk](https://www.theweek.co.uk/us/59331/roswell-ufo-crash-what-really-happened-67-years-ago)
4. [Autopsy FAQ - ufo.it](https://www.ufo.it/testi/autopsy-faq.htm)
5. [Roswell UFO Part 1 of 1 - vault.fbi.gov](https://vault.fbi.gov/Roswell%20UFO/Roswell%20UFO%20Part%201%20of%201/view)
6. [UFOs and the Guy Hottel Memo - fbi.gov](https://www.fbi.gov/news/stories/ufos-and-the-guy-hottel-memo)
7. [Project Blue Book - wikipedia.org](https://en.wikipedia.org/wiki/Project_Blue_Book)
8. [Majestic 12 - wikipedia.org](https://en.wikipedia.org/wiki/Majestic_12)
9. [The Roswell Files - roswellfiles.com](http://www.roswellfiles.com/index.htm)
10. [The Long Tail of Roswell: A Brief History of Our Most Famous Aliens - vice.com](https://www.vice.com/en/article/qkkywm/the-long-tail-of-roswell-a-brief-history-of-our-most-famous-aliens)
11. [The Ray Santilli video - youtube.com](https://www.youtube.com/watch?v=7dCoc3AUCmc)
12. [Roswell autopsy? - youtube.com](https://www.youtube.com/watch?v=UNeWpGitofQ)


