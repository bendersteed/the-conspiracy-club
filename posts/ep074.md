title: Επεισόδιο 74: Helena Blavatsky
author: The Conspiracy Club
email: info@theconspiracyclub.gr
number: 74
date: 2022-06-29 02:39
tags: blavatsky, theosophy, helena, yelena
summary: Μια γυναίκα που γύρισε όλον τον κόσμο για να ανακαλύψει τα μυστήρια της ζωής και που προσπάθησε να μοιραστεί ότι βρήκε. Ακούστε στο σημερινό για τα έργα και τις θεωρίες μιας από τις πιο σημαντικές φιγούρες στην αναγέννηση του μυστικισμού στον δυτικό κόσμο, της περιβόητης Μαντάμ Μπλαβάτσκι.
cover: https://theconspiracyclub.gr/assets/images/ep74.jpg
duration: "01:06:42"
enclosure: url:"https://theconspiracyclub.gr/assets/episodes/ep74.mp3" length:"32021307"
---
Μια γυναίκα που γύρισε όλον τον κόσμο για να ανακαλύψει τα μυστήρια της ζωής και που προσπάθησε να
μοιραστεί ότι βρήκε. Ακούστε στο σημερινό για τα έργα και τις θεωρίες μιας από τις πιο σημαντικές
φιγούρες στην αναγέννηση του μυστικισμού στον δυτικό κόσμο, της περιβόητης Μαντάμ Μπλαβάτσκι.

## Σχετικά links:
1. [Helena Blavatsky - wikipedia.org](https://en.wikipedia.org/wiki/Helena_Blavatsky)
2. [Helena Blavatsky - Russian Spiritualist - britannica.com](https://www.britannica.com/biography/Helena-Blavatsky)
3. [Helena Blavatsky: The idiosyncratic occultist who divided opinion - theneweuropean.co.uk](https://www.theneweuropean.co.uk/helena-blavatsky-great-european-lives/)
4. [Theosophical Society - wikipedia.org](https://en.wikipedia.org/wiki/Theosophical_Society)
