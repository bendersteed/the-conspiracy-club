;;; Copyright © 2020 Dimakakos Dimakis <me@bendersteed.tech>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (tcc static-pages)
  #:use-module (haunt utils)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt html)
  #:use-module (tcc utils)
  #:use-module (tcc theme)
  #:export (index-page
            404-page))

(define (index-page site posts)
  (make-page "index.html"
	     (base-tmpl site
			"")
	     sxml->html))

(define (404-page site posts)
  (make-page "404.html"
             (base-tmpl site
                        "This page doesn't exist")
             sxml->html))
