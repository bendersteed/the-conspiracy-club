;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>
;;; Copyright © 2020 Dimakakos Dimakis <me@bendersteed.tech>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.


(define-module (tcc blog)
  #:use-module (ice-9 match)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt utils)
  #:use-module (haunt html)
  #:use-module (haunt builder blog)
  #:use-module (tcc utils)
  #:export (blog-tcc))

(define (with-layout theme site title body metadata)
  ((theme-layout theme) site title body metadata))

(define (render-post theme site post posts)
  (let ((title (post-ref post 'title))
        (body ((theme-post-template theme) post posts))
	(metadata (social-metadata site post)))
    (with-layout theme site title body metadata)))

(define (render-collection theme site title posts prefix)
  (let ((body ((theme-collection-template theme) site title posts prefix))
	(metadata 'f))
    (with-layout theme site title body metadata)))

(define* (blog-tcc #:key (theme ugly-theme) prefix
               (collections
                `(("Recent Posts" "index.html" ,posts/reverse-chronological))))
  "Return a procedure that transforms a list of posts into pages
decorated by THEME, whose URLs start with PREFIX."
  (define (make-file-name base-name)
    (if prefix
        (string-append prefix "/" base-name)
        base-name))

  (lambda (site posts)
    (define (post->page post)
      (let ((base-name (string-append (site-post-slug site post)
                                      ".html")))
        (make-page (make-file-name base-name)
                   (render-post theme site post posts)
                   sxml->html)))

    (define collection->page
      (match-lambda
       ((title file-name filter)
        (make-page (make-file-name file-name)
                   (render-collection theme site title (filter posts) prefix)
                   sxml->html))))

    (append (map post->page posts)
            (map collection->page collections))))
