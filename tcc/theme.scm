;;; Copyright © 2020 Dimakakos Dimakis <me@bendersteed.tech>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (tcc theme)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt utils)
  #:use-module (haunt builder blog)
  #:use-module (haunt builder atom)
  #:use-module (tcc utils)
  #:export (tcc-haunt-theme
	    base-tmpl
	    js))

(define* (base-tmpl site body #:key title metadata)
  `((doctype html)
    (html (@ (lang "el")
             (data-theme "black"))
     (head
      (meta (@ (charset "utf-8")))
      (meta (@ (name "viewport")
	       (content "width=device-width, initial-scale=1")))
      (title ,(site-title site))
      ;; favicon
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "57x57")
               (href "/apple-touch-icon-57x57.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "114x114")
               (href "/apple-touch-icon-114x114.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "72x72")
               (href "/apple-touch-icon-72x72.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "144x144")
               (href "/apple-touch-icon-144x144.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "60x60")
               (href "/apple-touch-icon-60x60.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "120x120")
               (href "/apple-touch-icon-120x120.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "76x76")
               (href "/apple-touch-icon-76x76.png")))
      (link (@ (rel "apple-touch-icon-precomposed")
               (sizes "152x152")
               (href "/apple-touch-icon-152x152.png")))
      
      (link (@ (rel "icon")
               (type "image/png")
               (href "/favicon-196x196.png")
               (sizes "196x196")))
      
      (link (@ (rel "icon")
               (type "image/png")
               (href "/favicon-96x96.png")
               (sizes "96x96")))
      (link (@ (rel "icon")
               (type "image/png")
               (href "/favicon-32x32.png")
               (sizes "32x32")))
      (link (@ (rel "icon")
               (type "image/png")
               (href "/favicon-16x16.png")
               (sizes "16x16")))
      (link (@ (rel "icon")
               (type "image/png")
               (href "/favicon-128.png")
               (sizes "128x128")))
      (meta (@ (name "application-name")
               (content "The Conspiracy Club - Podcast")))
      (meta (@ (name "msapplication-TileColor")
               (content "#FFFFFF")) )
      (meta (@ (name "msapplication-TileImage")
               (content "/mstile-144x144.png")))
      (meta (@ (name "msapplication-square70x70logo")
               (content "/mstile-70x70.png")))
      (meta (@ (name "msapplication-square150x150logo")
               (content "/mstile-150x150.png")))
      (meta (@ (name "msapplication-wide310x150logo")
               (content "/mstile-310x150.png")))
      (meta (@ (name "msapplication-square310x310logo")
               (content "/mstile-310x310.png")))
      ;; css
      (link (@ (rel "stylesheet")
               (href "/build/bundle.css")))
      (link (@ (rel "stylesheet")
               (href "/index.css")))

      ;; feeds
      (link (@ (rel "alternate")
	       (title "The Conspiracy Club")
	       (type "application/rss+xml")
	       (href "/rss-itunes.xml")))
      (link (@ (rel "alternate")
	       (title "The Conspiracy Club")
	       (type "application/atom+xml")
	       (href "/feed.xml")))
      ;; social metadata
      (meta (@ (property "og:title")
	       (content ,(if title
			     (string-append title " | The Conspiracy Club")
			     (site-title site)))))
      (meta (@ (property "og:description")
	       (content ,(if metadata
			     (assoc-ref metadata 'description)
			     "Talking casualy about conspiracies, in Greek."))))
      (meta (@ (property "og:image")
	       (content ,(if metadata
			     (assoc-ref metadata 'image)
			     "https://theconspiracyclub.gr/assets/images/social.jpg"))))
      (meta (@ (property "og:url")
	       (content ,(if metadata
			     (assoc-ref metadata 'url)
			     "https://theconspiracyclub.gr"))))
      (meta (@ (name "twitter:card")
	       (content "summary_large_image")))
      (meta (@ (property "og:site_title")
	       (content "The Conspiracy Club")))
      (meta (@ (name "twitter:image:alt")
	       (content "Logo of our website or cover image for our post.")))
      (meta (@ (property "fb:app_id")
	       (content "")))
      (meta (@ (name "twitter:site")
	       (content "@CClub51")))

      (script (@ (defer "")
                 (src "/build/bundle.js")))
))
    (body (@ (class "bg-repeat")
             (style "background-image: url('/assets/img/3px-tile.png')"))
          ,body
          (script (@  (data-goatcounter "https://theconspiracyclub.goatcounter.com/count")
                      (async "")
                      (src "//gc.zgo.at/count.js"))))))

(define (post-template post posts)
  (let ((enclosure (post-ref post 'enclosure)))
   `(noscript
     (h1 "The Conspiracy Club")
     (h2 ,(post-ref post 'title))
     (audio (@ (controls "")
               (src ,(enclosure-url enclosure))))
     (p ,(post-sxml post)))))

(define (collection-template site title posts prefix)
  '())

(define tcc-haunt-theme
  (theme #:name "The Conspiracy Club"
	 #:layout
	 (lambda (site title body metadata)
	   (base-tmpl
	    site body
	    #:title title
	    #:metadata metadata))
	 #:post-template post-template
	 #:collection-template collection-template))
