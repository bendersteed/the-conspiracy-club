;;; Copyright © 2020 Dimakakos Dimakis <me@bendersteed.tech>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

;;; RSS feed builder for itunes
(define-module (tcc rss-itunes)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (sxml simple)
  #:use-module (haunt site)
  #:use-module (haunt html)
  #:use-module (haunt utils)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt builder atom)
  #:use-module (tcc utils)
  #:export (itunes-feed))

(define (date->rfc822-str date)
  (date->string date "~a, ~d ~b ~Y ~T ~z"))

(define (sxml->xml* sxml port)
  "Write SXML to PORT, preceded by an <?xml> tag."
  (display "<?xml version=\"1.0\" encoding=\"utf-8\"?>" port)
  (sxml->xml sxml port))

(define* (itunes-post->rss-item site post #:key (blog-prefix "")) ;;TODO: make itunes complian rss items
  "Convert POST into an RSS <item> node."
  `(item
    (title ,(post-ref post 'title))
    ;; Looks like: <author>lawyer@boyer.net (Lawyer Boyer)</author>
    (author
     ,(let ((email (post-ref post 'email))
            (author (post-ref post 'author)))
        (string-append (if email
                           (string-append email " ")
                           "")
                       (if author
                           (string-append "(" author ")")
                           ""))))
    (pubDate ,(date->rfc822-str (post-date post)))
    (link ,(make-uri site #:link (post-uri site post)))
    (description ,(sxml->html-string (post-sxml post)))
    ,@(map (lambda (enclosure)
             `(enclosure (@ (title ,(enclosure-title enclosure))
                            (url ,(enclosure-url enclosure))
                            (type ,(enclosure-mime-type enclosure))
                            ,@(map (match-lambda
                                     ((key . value)
                                      (list key value)))
                                   (enclosure-extra enclosure)))))
           (post-ref-all post 'enclosure))
    (itunes:duration ,(post-ref post 'duration))
    (itunes:summary ,(post-ref post 'summary))
    (itunes:image (@ (href ,(post-ref post 'cover))))))

(define* (itunes-feed #:key 
                   (file-name "rss-itunes.xml")
                   (subtitle "Recent Posts")
		   (author "Anonymous")
		   (owner-name "Anonymous")
		   (owner-email "anonymous@nomail.com")
		   (summary "WTF")
		   (explicit? #f)
		   (image "/#")
		   (category "null")
                   (filter posts/reverse-chronological)
                   (max-entries 20)
                   (blog-prefix ""))
  "Return a builder procedure that renders a list of posts as an RSS
feed.  All arguments are optional:

FILE-NAME: The page file name
SUBTITLE: The feed subtitle
FILTER: The procedure called to manipulate the posts list before rendering
MAX-ENTRIES: The maximum number of posts to render in the feed"
  (lambda (site posts)
    (setlocale LC_ALL "en_US.UTF-8")
    (make-page file-name
               `(rss (@ (version "2.0")
			(xmlns:itunes "http://www.itunes.com/dtds/podcast-1.0.dtd")
			(atom "http://www.w3.org/2005/Atom"))
                     (channel
                      (title ,(site-title site))
		      (link ,(make-uri site))
                      (description ,subtitle)
		      (language "el")
		      (copyright "Licensed under CC BY-SA 4.0 International")
		      (itunes:author ,author)
		      (itunes:owner (itunes:name ,owner-name)
				    (itunes:email ,owner-email))
		      (itunes:summary ,summary)
		      ,(if explicit?
			   `(itunes:explicit "yes")
			   `(itunes:explicit "no"))
		      (itunes:image (@ (href ,(make-uri site
							#:link image))))
		      (itunes:category (@ (text ,category)))
                      (pubDate ,(date->rfc822-str (current-date)))
		      (lastBuildDate ,(date->rfc822-str (current-date)))
                      (link (@ (href ,(make-uri site))))
                      ,@(map (cut itunes-post->rss-item site <>
                                  #:blog-prefix blog-prefix)
                             (take-up-to max-entries (filter posts)))))
               sxml->xml*)))


