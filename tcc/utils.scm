;;; Copyright © 2020 Dimakakos Dimakis <me@bendersteed.tech>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (tcc utils)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt utils)
  #:use-module (haunt html)
  #:use-module (haunt builder blog)
  #:use-module (haunt builder atom)
  #:use-module (json)
  #:export (post-uri
	    audio-source
	    make-uri
	    social-metadata
	    make-episodes-json))


;; general utils
(define (post-uri site post)
  (string-append "/episodes/" (site-post-slug site post) ".html"))

(define (audio-source enclosure)
  `(source (@ (src ,(enclosure-url enclosure))
              (type ,(enclosure-mime-type enclosure)))))

(define* (make-uri site #:key (link ""))
  (string-append "https://"
		 (site-domain site) link))

(define (social-metadata site post)
  `((description . ,(post-ref post 'summary))
    (image . ,(post-ref post 'cover))
    (url . ,(make-uri site #:link (post-uri site post)))))

;; metadata to json utils for front-page player
(define (make-episode-scm site post)
  (let ((enclosure (post-ref post 'enclosure)))
    `((number . ,(post-ref post 'number))
      (name . ,(post-ref post 'title))
      (info . ,(post-ref post 'summary))
      (url . ,(enclosure-url enclosure))
      (cover_art_url . ,(post-ref post 'cover))
      (date . ,(date->string* (post-date post)))
      (html . ,(sxml->html-string (post-sxml post)))
      (link . ,(post-uri site post))
      (tags . ,(list->vector (post-ref post 'tags))))))

(define (make-episodes-json site posts)
  (make-page  "/assets/js/episodes.json"
	      `((songs . ,(list->vector
			   (map (lambda (x) (make-episode-scm site x))
				(posts/reverse-chronological posts)))))
	      scm->json))

