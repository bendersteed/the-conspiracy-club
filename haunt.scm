(use-modules (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
	     (haunt builder rss)
             (haunt reader)
	     (haunt reader commonmark)
             (haunt site)
	     (haunt utils)
	     (tcc theme)
	     (tcc utils)
	     (tcc blog)
             (tcc static-pages)
	     (tcc rss-itunes))

(define tcc-subtitle
  "Talking casualy about conspiracies, in Greek.")

(define tcc-logo
  "/assets/images/social.jpg")

(setlocale LC_ALL "el_GR.UTF-8")

(site #:title "The Conspiracy Club"
      #:domain "theconspiracyclub.gr"
      #:default-metadata
      '((author . "bendersteed")
        (email  . "bendersteed@gmail.com"))
      #:readers (list commonmark-reader html-reader)
      #:builders (list
		  (blog-tcc #:prefix "/episodes"
			    #:theme tcc-haunt-theme)
		  make-episodes-json
		  (itunes-feed #:subtitle tcc-subtitle
			       #:author "The Conspiracy Club"
			       #:owner-name "The Conspiracy Club"
			       #:owner-email "info@theconspiracyclub.gr"
			       #:summary tcc-subtitle
			       #:explicit? 't
			       #:image tcc-logo
			       #:category "Society & Culture"
			       #:max-entries 1024
			       #:blog-prefix "/episodes")
		  (atom-feed)
                  index-page
                  404-page
		  (static-directory "assets" "assets")))
